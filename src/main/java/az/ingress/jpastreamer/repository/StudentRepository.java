package az.ingress.jpastreamer.repository;

import az.ingress.jpastreamer.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student , Long>{




    @Query(value = "select * from student where student.surname like %:surname and student.address like %:address",nativeQuery = true)
    List<Student> getByEndTwoCharactersAndEqualsCity(String surname, String address);


    @Query(value = "select s from Student s  where s.surname like %:surname and s.address =:address")
    List<Student>  getByEndTwoCharactersAndEqualsCityJPQl(String surname,String address);

}
