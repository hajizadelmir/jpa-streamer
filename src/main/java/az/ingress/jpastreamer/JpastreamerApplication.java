package az.ingress.jpastreamer;

import az.ingress.jpastreamer.model.Student;
import az.ingress.jpastreamer.model.Student$;
import az.ingress.jpastreamer.repository.StudentRepository;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
@EnableConfigurationProperties
public class JpastreamerApplication implements CommandLineRunner {
	private final StudentRepository studentRepository;
	private final EntityManagerFactory entityManagerFactory;

	public static void main(String[] args) {
		SpringApplication.run(JpastreamerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//nativeQuery
		studentRepository.getByEndTwoCharactersAndEqualsCity("ov","Baku").stream().forEach(System.out::println);
		//JPQl
		studentRepository.getByEndTwoCharactersAndEqualsCityJPQl("ov","Baku").stream().forEach(System.out::println);


		//JPA Streamer
		JPAStreamer jpaStreamer = JPAStreamer.of(entityManagerFactory);
		jpaStreamer.stream(Student.class)
				.filter(Student$.surname.endsWith("ov"))
				.filter(Student$.address.contains("Baku"))
				.forEach(System.out::println);






	}
}
